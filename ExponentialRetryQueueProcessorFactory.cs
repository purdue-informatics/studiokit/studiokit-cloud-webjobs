﻿using Azure.Storage.Queues.Models;
using Microsoft.Azure.WebJobs.Host.Executors;
using Microsoft.Azure.WebJobs.Host.Queues;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Cloud.WebJobs;

public class ExponentialRetryQueueProcessorFactory : IQueueProcessorFactory
{
	public QueueProcessor Create(QueueProcessorOptions queueProcessorOptions)
	{
		return new CustomQueueProcessor(queueProcessorOptions);
	}

	private class CustomQueueProcessor : QueueProcessor
	{
		protected internal CustomQueueProcessor(QueueProcessorOptions queueProcessorOptions) : base(queueProcessorOptions)
		{
		}

		protected override async Task ReleaseMessageAsync(QueueMessage message, FunctionResult result, TimeSpan visibilityTimeout,
			CancellationToken cancellationToken)
		{
			// VisibilityTimeout ^ DequeueCount
			var exponentialVisibilityTimeout = TimeSpan.FromSeconds(Math.Pow(visibilityTimeout.TotalSeconds, message.DequeueCount));
			await base.ReleaseMessageAsync(message, result, exponentialVisibilityTimeout, cancellationToken);
		}
	}
}
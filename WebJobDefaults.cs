namespace StudioKit.Cloud.WebJobs;

public static class WebJobDefaults
{
	public const int MaxDequeueCount = 5;
	public const int VisibilityTimeoutSeconds = 4;
	public const int BatchSize = 1;
}
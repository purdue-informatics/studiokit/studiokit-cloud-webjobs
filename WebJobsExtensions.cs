﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Azure.Storage.Queues;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host.Queues;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.EnvironmentVariables;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using StudioKit.Configuration;
using StudioKit.Encryption;
using StudioKit.ErrorHandling;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.Security.BusinessLogic.Interfaces;
using StudioKit.Security.BusinessLogic.Services;
using StudioKit.TransientFaultHandling.Http;
using StudioKit.Utilities;
using StudioKit.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace StudioKit.Cloud.WebJobs;

public static class WebJobsExtensions
{
	public static IHostBuilder ConfigureWebJobServices(
		this IHostBuilder hostBuilder,
		Assembly applicationOrWebJobAssembly,
		Action<ContainerBuilder> configureContainerAction)
	{
		hostBuilder
			.UseServiceProviderFactory(new AutofacServiceProviderFactory())
			.ConfigureServices((context, services) =>
			{
				EncryptedConfigurationManager.Configuration = context.Configuration;

				services.AddHttpClient(string.Empty)
					.ConfigurePrimaryHttpMessageHandler<DecompressingHttpClientHandler>();
				services.AddSingleton<IQueueProcessorFactory>(new ExponentialRetryQueueProcessorFactory());
			})
			.ConfigureLogging((_, loggingBuilder) =>
			{
				loggingBuilder.AddSentry(o =>
				{
					var release = new Release(applicationOrWebJobAssembly);
					o.Dsn = EncryptedConfigurationManager.GetSetting(BaseAppSetting.SentryDsn);
					o.Release = release.ReleaseName;
					o.Environment = release.Environment;
				});

				loggingBuilder.AddApplicationInsightsWebJobs();
			})
			.ConfigureContainer<ContainerBuilder>(builder =>
			{
				builder.Register(_ => new Release(applicationOrWebJobAssembly))
					.As<IRelease>()
					.SingleInstance();

				builder.RegisterType<DecompressingHttpClientHandler>()
					.AsSelf();

				ErrorHandler.Instance = new SentryErrorHandler();
				builder.RegisterInstance(ErrorHandler.Instance)
					.As<IErrorHandler>()
					.ExternallyOwned();

				builder.RegisterType<DateTimeProvider>()
					.As<IDateTimeProvider>()
					.SingleInstance();

				builder.RegisterType<SystemPrincipalProvider>()
					.As<IPrincipalProvider>()
					.SingleInstance();

				configureContainerAction(builder);
			})
			// This abomination is due to a bug in ConfigureWebJobs above. Discussion is here: https://github.com/Azure/azure-webjobs-sdk/issues/1931
			// Without it, values in appsettings.{Environment}.json won't be respected
			.ConfigureServices((context, _) =>
			{
				var configProviders = ((context.Configuration as ConfigurationRoot)?.Providers as List<IConfigurationProvider>);

				if (configProviders?.Count(c =>
						c.GetType() == typeof(JsonConfigurationProvider) &&
						(c as JsonConfigurationProvider)?.Source.Path == "appsettings.json") > 1)
				{
					configProviders.Remove(configProviders.Last(c =>
						c.GetType() == typeof(JsonConfigurationProvider) &&
						(c as JsonConfigurationProvider)?.Source.Path == "appsettings.json"));
				}

				if (configProviders?.Count(c => c.GetType() == typeof(EnvironmentVariablesConfigurationProvider)) > 1)
				{
					configProviders.Remove(configProviders.Last(c => c.GetType() == typeof(EnvironmentVariablesConfigurationProvider)));
				}
			});

		return hostBuilder;
	}

	public static void ConfigureWebJobs(IWebJobsBuilder webJobsBuilder, int? queueMaxDequeueCount = null, int? queuesBatchSize = null)
	{
		webJobsBuilder.AddAzureStorageCoreServices();
		webJobsBuilder.AddAzureStorageQueues(queuesOptions =>
		{
			queuesOptions.MessageEncoding = QueueMessageEncoding.None;
			queuesOptions.MaxDequeueCount = queueMaxDequeueCount ?? WebJobDefaults.MaxDequeueCount;
			queuesOptions.VisibilityTimeout = TimeSpan.FromSeconds(WebJobDefaults.VisibilityTimeoutSeconds);
			queuesOptions.BatchSize = queuesBatchSize ?? WebJobDefaults.BatchSize;
		});
	}
}